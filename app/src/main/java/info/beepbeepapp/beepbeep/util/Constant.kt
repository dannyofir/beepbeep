package info.beepbeepapp.beepbeep.util

class Constant {
    companion object {
        const val MAX_NUM_OF_SELECTED_CONTACTS = 4
        const val EDIT_CONTACT_KEY = "edit_contact"
        const val CONTACTS_TO_SUMMARY_FRAGMENT_KEY = "contacts_to_summary_fragment_key"
    }
}