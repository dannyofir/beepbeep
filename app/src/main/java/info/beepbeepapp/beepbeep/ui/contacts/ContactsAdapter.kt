package info.beepbeepapp.beepbeep.ui.contacts

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import info.beepbeepapp.beepbeep.BeepBeepApplication
import info.beepbeepapp.beepbeep.R
import info.beepbeepapp.beepbeep.model.Contact
import info.beepbeepapp.beepbeep.util.Constant.Companion.MAX_NUM_OF_SELECTED_CONTACTS
import kotlinx.android.synthetic.main.contacts_list_item.view.*

class ContactsAdapter(val listener: ContactsListListener) :
    ListAdapter<Contact, RecyclerView.ViewHolder>(ContactCallback()) {

    var contactList: MutableList<Contact>? = null
    var selectedContactList = ArrayList<Contact>()
    var isInSelectionMode: Boolean = false

    class ContactCallback : DiffUtil.ItemCallback<Contact>() {

        override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.primaryPhoneNumber == newItem.primaryPhoneNumber
        }

        override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.fullName == newItem.fullName
        }

    }

    interface ContactsListListener {
        fun contactSelected(selectedContacts: ArrayList<Contact>)
        fun contactClicked(contact: Contact)
    }

    override fun submitList(list: MutableList<Contact>?) {
        contactList = list
        super.submitList(list)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contacts_list_item, parent, false)
        val holder = ContactItemViewHolder(view)
        holder.itemView.setOnLongClickListener {
            if (selectedContactList.size < MAX_NUM_OF_SELECTED_CONTACTS) {
                val contact = contactList?.get(holder.adapterPosition)!!
                isInSelectionMode = true
                selectedContactList.add(contact)
                listener.contactSelected(selectedContactList)
                it.setBackgroundColor(if (selectedContactList.contains(contact)) BeepBeepApplication.applicationContext().resources.getColor(R.color.contact_selection_gray) else Color.WHITE)
            }
            true
        }
        holder.itemView.setOnClickListener {
            val contact = contactList?.get(holder.adapterPosition)!!
            if (isInSelectionMode) {
                contactClicked(contact)
                it.setBackgroundColor(if (selectedContactList.contains(contact)) BeepBeepApplication.applicationContext().resources.getColor(R.color.contact_selection_gray) else Color.WHITE)
            } else {
                listener.contactClicked(contact)
            }
            true
        }
        return holder
    }

    private fun contactClicked(contact: Contact) {
        if (selectedContactList.size < MAX_NUM_OF_SELECTED_CONTACTS) {
            if (selectedContactList.contains(contact)) selectedContactList.remove(contact) else selectedContactList.add(contact)
            if (selectedContactList.size == 0) isInSelectionMode = false
        } else {
            if (selectedContactList.contains(contact)) selectedContactList.remove(contact)
        }
        listener.contactSelected(selectedContactList)
    }

    fun updateSelectedContacts(selectedContacts: MutableList<Contact>) {
        isInSelectionMode = true
        if (selectedContactList.isEmpty()) {
            selectedContactList.addAll(selectedContacts)
            notifyDataSetChanged()
        }
    }

    fun clearSelectedContacts() {
        isInSelectionMode = false
        selectedContactList = arrayListOf()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val contact = getItem(position)

        holder.itemView.setBackgroundColor(if (selectedContactList.contains(contact)) BeepBeepApplication.applicationContext().resources.getColor(R.color.contact_selection_gray) else Color.WHITE)
        (holder as ContactItemViewHolder).bind(contact, position)
    }

    class ContactItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val contactName: AppCompatTextView = itemView.contact_name
        val contactPhoto: AppCompatImageView = itemView.contact_photo
        val contactAddress: AppCompatImageView = itemView.contact_address

        fun bind(contact: Contact, position: Int) {
            contactName.text = contact.fullName
            if (contact.photoUri != null) {
                Glide.with(contactPhoto)
                    .load(contact.photoUri)
                    .apply(RequestOptions.circleCropTransform())
                    .into(contactPhoto)
            } else {

                val generator: ColorGenerator = ColorGenerator.MATERIAL

                val textDrawable = TextDrawable.builder()
                    .buildRound(contact.fullName[0].toString(), generator.getColor(contact.primaryPhoneNumber))

                contactPhoto.setImageDrawable(textDrawable)

            }
            if (contact.address.isNullOrEmpty()) {
                contactAddress.visibility = View.INVISIBLE
            } else {
                contactAddress.visibility = View.VISIBLE
            }
        }

    }

}