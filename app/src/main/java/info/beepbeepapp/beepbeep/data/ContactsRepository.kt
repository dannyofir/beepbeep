package info.beepbeepapp.beepbeep.data

import info.beepbeepapp.beepbeep.model.Contact
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactsRepository @Inject constructor(val contactsLocalDataSource: ContactsLocalDataSource) {

    val cachedContactsList = mutableListOf<Contact>()

    fun getContacts() : Observable<MutableList<Contact>> {
        return Maybe.concat(getCachedContacts(), getContactsFromDb(), getPhoneContacts()).first(cachedContactsList).toObservable()
    }

    fun editContact(contact: Contact): Completable {
        return contactsLocalDataSource.editContact(contact).doOnComplete {
//            getContactsFromDb()
        }
    }

    fun getCachedContacts() : Maybe<MutableList<Contact>> {
        return Maybe.create<MutableList<Contact>> { emitter ->
            if (!cachedContactsList.isEmpty()) {
                emitter.onSuccess(cachedContactsList)
            }
            emitter.onComplete()
        }
    }

    private fun getContactsFromDb() : Maybe<MutableList<Contact>> {
        return contactsLocalDataSource.getContactsFromDb().doOnSuccess {
            cachedContactsList.clear()
            cachedContactsList.addAll(it)
        }
    }

    private fun getPhoneContacts() : Maybe<MutableList<Contact>> {
        return contactsLocalDataSource.getPhoneContactsAndInsert().doOnSuccess {
            if (cachedContactsList.isEmpty()) {
                cachedContactsList.addAll(it)
            }
        }
    }

}