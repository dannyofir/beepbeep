package info.beepbeepapp.beepbeep.data

import android.app.Application
import android.content.Context
import com.here.android.mpa.common.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class MapRepository @Inject constructor(val application: Application): OnEngineInitListener, PositioningManager.OnPositionChangedListener {

    val subject = BehaviorSubject.create<GeoCoordinate>()
    private var positioningManager: PositioningManager? = null

    init {
        MapEngine.getInstance().init(application, this)
    }

    fun getGeoCoordinates() : Observable<GeoCoordinate> {
        positioningManager?.start(PositioningManager.LocationMethod.GPS_NETWORK)
        positioningManager?.addListener(WeakReference<PositioningManager.OnPositionChangedListener>(this))
        return subject
    }

    override fun onEngineInitializationCompleted(p0: OnEngineInitListener.Error?) {
        initPositionManager()
    }

    fun initPositionManager() {
        positioningManager = PositioningManager.getInstance()
        positioningManager?.start(PositioningManager.LocationMethod.GPS_NETWORK)
        positioningManager?.addListener(WeakReference<PositioningManager.OnPositionChangedListener>(this))
    }

    override fun onPositionFixChanged(p0: PositioningManager.LocationMethod?, p1: PositioningManager.LocationStatus?) {

    }

    override fun onPositionUpdated(p0: PositioningManager.LocationMethod?, p1: GeoPosition?, p2: Boolean) {
        Timber.d("explore onPositionUpdated: " + p1?.coordinate.toString())
        positioningManager?.removeListener(this)
        val geoCoordinate = p1?.coordinate
        subject.onNext(geoCoordinate!!)
    }

}