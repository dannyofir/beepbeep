package info.beepbeepapp.beepbeep.ui.explore

import androidx.lifecycle.MutableLiveData
import com.here.android.mpa.common.GeoCoordinate
import info.beepbeepapp.beepbeep.base.BaseViewModel
import info.beepbeepapp.beepbeep.data.MapRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class ExploreViewModel @Inject constructor(private val mapRepository: MapRepository) : BaseViewModel() {

    val geoCoordinate = MutableLiveData<GeoCoordinate>()

    init {
        getGeoCoordinates()
    }

    private fun getGeoCoordinates() {
        mapRepository.getGeoCoordinates()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                compositeDisposable.add(it)
            }
            .subscribe {
                Timber.d("exploreViewModel geocoordinate emitted: " + it.toString() + this.toString())
                geoCoordinate.value = it
            }
    }

}
