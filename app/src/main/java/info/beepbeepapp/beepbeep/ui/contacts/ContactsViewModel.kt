package info.beepbeepapp.beepbeep.ui.contacts

import androidx.lifecycle.MutableLiveData
import info.beepbeepapp.beepbeep.base.BaseViewModel
import info.beepbeepapp.beepbeep.data.ContactsRepository
import info.beepbeepapp.beepbeep.model.Contact
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.function.Predicate
import javax.inject.Inject

class ContactsViewModel @Inject constructor(private val contactsRepository: ContactsRepository) : BaseViewModel() {

    val contactsList = MutableLiveData<MutableList<Contact>>()
    val selectedContactsList = MutableLiveData<ArrayList<Contact>>()
    val loading = MutableLiveData<Boolean>()

    init {
        getContacts()
    }

    fun queryContacts(query: String) {
        contactsRepository.getCachedContacts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                compositeDisposable.add(it)
            }
            .flatMapObservable { Observable.fromIterable(it) }
            .filter { it.fullName.toLowerCase().contains(query.toLowerCase()) }
            .toList()
            .toMaybe()
            .subscribe {
                contactsList.value = it
            }
    }

    private fun getContacts() {
        contactsRepository.getContacts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                loading.value = true
                compositeDisposable.add(it)
            }
            .subscribe { t: MutableList<Contact>? ->
                Timber.d("getContacts: " + t.toString())
                loading.value = false
                contactsList.value = t
            }
    }

}
