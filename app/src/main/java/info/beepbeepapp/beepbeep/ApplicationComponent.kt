package info.beepbeepapp.beepbeep

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import info.beepbeepapp.beepbeep.di.ViewModelModule
import info.beepbeepapp.beepbeep.di.module.ActivityBindingModule
import info.beepbeepapp.beepbeep.di.module.FragmentBindingModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
    ActivityBindingModule::class,
    ViewModelModule::class,
    ApplicationModule::class])
interface ApplicationComponent : AndroidInjector<BeepBeepApplication> {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun create(application: Application):Builder
        fun build(): ApplicationComponent

    }

}