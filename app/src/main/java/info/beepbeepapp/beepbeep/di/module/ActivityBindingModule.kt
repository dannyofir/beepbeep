package info.beepbeepapp.beepbeep.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import info.beepbeepapp.beepbeep.ui.MainActivity

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [FragmentBindingModule::class])
    abstract fun mainActivity(): MainActivity


}