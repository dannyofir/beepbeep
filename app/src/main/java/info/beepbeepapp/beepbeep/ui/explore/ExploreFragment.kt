package info.beepbeepapp.beepbeep.ui.explore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.here.android.mpa.common.PositioningManager
import com.here.android.mpa.mapping.Map
import dagger.android.support.DaggerFragment
import info.beepbeepapp.beepbeep.R
import kotlinx.android.synthetic.main.explore_fragment.*
import timber.log.Timber
import javax.inject.Inject


class ExploreFragment : DaggerFragment() {

    companion object {
        fun newInstance() = ExploreFragment()
    }

    private lateinit var viewModel: ExploreViewModel
    private lateinit var map: Map

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.explore_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ExploreViewModel::class.java)
        viewModel.geoCoordinate.observe(this, Observer {
            map_view.map ?: let {
                map_view.map = Map()
            }
            map_view.map.positionIndicator.isVisible = true
            map_view.map.setCenter(it, Map.Animation.LINEAR)
            PositioningManager.getInstance().stop()
        })
    }

}
