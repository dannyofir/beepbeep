package info.beepbeepapp.beepbeep

import android.app.Application
import android.content.ContentResolver

import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import dagger.Module
import dagger.Provides
import info.beepbeepapp.beepbeep.data.db.ApplicationDatabase
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun provideSharedPreference(application: Application): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(application)

    @Provides
    @Singleton
    fun provideContentResolver(application: Application): ContentResolver =
        application.contentResolver

    @Provides
    @Singleton
    fun provideApplicationDatabase(application: Application): ApplicationDatabase =
            Room.databaseBuilder(application, ApplicationDatabase::class.java, "application-database").build()

}