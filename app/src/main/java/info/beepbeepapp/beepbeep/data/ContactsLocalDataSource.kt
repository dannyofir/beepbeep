package info.beepbeepapp.beepbeep.data

import android.content.ContentResolver
import android.provider.ContactsContract
import info.beepbeepapp.beepbeep.data.db.ApplicationDatabase
import info.beepbeepapp.beepbeep.model.Contact
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class ContactsLocalDataSource @Inject constructor(val applicationDatabase: ApplicationDatabase, val contentResolver: ContentResolver) {

    fun getContactsFromDb() : Maybe<MutableList<Contact>> {
        return applicationDatabase.contactDao().getUsers().filter {
            it.isNotEmpty() }
            .doOnSuccess {
                Timber.d("lalala")
            }
    }

    fun getPhoneContactsAndInsert() : Maybe<MutableList<Contact>> {
        return Single.fromCallable { getContactsFromContentProvider() }.filter {
            it.isNotEmpty() }
            .doOnSuccess {
                applicationDatabase.contactDao().insertAll(it) }
    }

    fun getPhoneContactsAndUpdate() : Maybe<MutableList<Contact>> {
        return Single.fromCallable { getContactsFromContentProvider() }.filter {
            it.isNotEmpty() }
            .doOnSuccess {
                applicationDatabase.contactDao().updateAll(it) }
    }

    fun editContact(contact: Contact) : Completable {
        return Completable.fromCallable { applicationDatabase.contactDao().update(contact) }
    }

    private fun getContactsFromContentProvider(): MutableList<Contact> {
        val contacts = mutableListOf<Contact>()
        val resolver: ContentResolver = contentResolver
        val cursor = resolver.query(
            ContactsContract.Contacts.CONTENT_URI, null, null, null,
            null)

        if (cursor.count > 0) {
            while (cursor.moveToNext()) {
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val phoneNumber = (cursor.getString(
                    cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))).toInt()
                val imageUri = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))

                if (phoneNumber > 0) {
                    val phoneNumbers = mutableListOf<String>()
                    val cursorPhone = contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", arrayOf(id), null)

                    if(cursorPhone.count > 0) {
                        while (cursorPhone.moveToNext()) {
                            val phoneNumValue = cursorPhone.getString(
                                cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            phoneNumbers.add(phoneNumValue)
                        }
                    }
                    cursorPhone.close()
                    contacts.add(Contact(id, name, phoneNumbers, phoneNumbers[0], imageUri))
                }
            }
        } else {
            //   toast("No contacts available!")
        }
        cursor.close()
        return contacts
    }

}