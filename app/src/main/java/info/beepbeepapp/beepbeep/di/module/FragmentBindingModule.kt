package info.beepbeepapp.beepbeep.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import info.beepbeepapp.beepbeep.ui.contacts.ContactsFragment
import info.beepbeepapp.beepbeep.ui.contacts.EditContactFragment
import info.beepbeepapp.beepbeep.ui.explore.ExploreFragment
import info.beepbeepapp.beepbeep.ui.recent.RecentFragment
import info.beepbeepapp.beepbeep.ui.summary.SummaryFragment

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun exploreFragment(): ExploreFragment

    @ContributesAndroidInjector
    abstract fun recentFragment(): RecentFragment

    @ContributesAndroidInjector
    abstract fun contactsFragment(): ContactsFragment

    @ContributesAndroidInjector
    abstract fun editContactFragment(): EditContactFragment

    @ContributesAndroidInjector
    abstract fun summaryFragment(): SummaryFragment


}