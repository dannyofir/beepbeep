package info.beepbeepapp.beepbeep.ui.summary

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import dagger.android.support.DaggerFragment

import info.beepbeepapp.beepbeep.R
import javax.inject.Inject
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import info.beepbeepapp.beepbeep.ui.MainActivity
import info.beepbeepapp.beepbeep.util.Constant
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.summary_fragment.*


class SummaryFragment : DaggerFragment() {

    companion object {
        fun newInstance() = SummaryFragment()
    }

    private lateinit var viewModel: SummaryViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.summary_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SummaryViewModel::class.java)
        // TODO: Use the ViewModel

        summary_list.layoutManager = LinearLayoutManager(context)
        val summaryAdapter = SummaryAdapter()
        summary_list.adapter = summaryAdapter
        val callback = SimpleItemTouchHelperCallback(summaryAdapter)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(summary_list)

        viewModel.contacts.value = arguments?.getParcelableArrayList(Constant.CONTACTS_TO_SUMMARY_FRAGMENT_KEY)

        viewModel.contacts.observe(this, Observer {
            summaryAdapter.submitList(it)
        })

    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).bottom_nav.visibility = View.GONE
    }

    override fun onDestroyView() {
        (activity as MainActivity).bottom_nav.visibility = View.VISIBLE
        super.onDestroyView()
    }

}
