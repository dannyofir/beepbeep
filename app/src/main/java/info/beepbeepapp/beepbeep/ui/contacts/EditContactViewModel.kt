package info.beepbeepapp.beepbeep.ui.contacts

import androidx.lifecycle.MutableLiveData
import info.beepbeepapp.beepbeep.base.BaseViewModel
import info.beepbeepapp.beepbeep.data.ContactsRepository
import info.beepbeepapp.beepbeep.model.Contact
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class EditContactViewModel @Inject constructor(private val contactsRepository: ContactsRepository): BaseViewModel() {

    val contact = MutableLiveData<Contact>()
    val saving = MutableLiveData<Boolean>()

    fun editContact() {
        saving.value = true
        contactsRepository.editContact(contact.value!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                compositeDisposable.add(it)
            }
            .subscribe {
                saving.value = false
            }
    }
}
