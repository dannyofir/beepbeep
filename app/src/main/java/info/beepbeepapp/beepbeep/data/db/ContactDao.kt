package info.beepbeepapp.beepbeep.data.db

import androidx.room.*
import info.beepbeepapp.beepbeep.model.Contact
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface ContactDao {

    @Query("SELECT * FROM contacts")
    fun getUsers(): Single<MutableList<Contact>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(contact: Contact)

    @Update
    fun update(contact: Contact)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(contacts: List<Contact>)

    @Update
    fun updateAll(contacts: List<Contact>)
}