package info.beepbeepapp.beepbeep.ui.summary

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import info.beepbeepapp.beepbeep.BeepBeepApplication
import info.beepbeepapp.beepbeep.R
import info.beepbeepapp.beepbeep.model.Contact
import kotlinx.android.synthetic.main.summary_list_item.view.*
import java.util.*

class SummaryAdapter: ListAdapter<Contact, RecyclerView.ViewHolder>(SummaryCallback()), SimpleItemTouchHelperCallback.ItemTouchHelperAdapter {

    var summaryList: MutableList<Contact>? = null

    class SummaryCallback : DiffUtil.ItemCallback<Contact>() {

        override fun areItemsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.primaryPhoneNumber == newItem.primaryPhoneNumber
        }

        override fun areContentsTheSame(oldItem: Contact, newItem: Contact): Boolean {
            return oldItem.fullName == newItem.fullName
        }

    }

//    interface SummaryListListener {
//        fun onStartDrag(holder: RecyclerView.ViewHolder)
//    }

    override fun submitList(list: MutableList<Contact>?) {
        summaryList = list
        super.submitList(list)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(summaryList, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(summaryList, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.summary_list_item, parent, false)
        val holder = SummaryItemViewHolder(view)
//        holder.itemView.setOnTouchListener { v, event ->
//            if (event.actionMasked == MotionEvent.ACTION_DOWN) {
//                listener.onStartDrag(holder)
//            }
//            false
//        }
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val contact = getItem(position)
        (holder as SummaryItemViewHolder).bind(contact, position)
    }

    class SummaryItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val contactName: AppCompatTextView = itemView.contact_name
        val contactPhoto: AppCompatImageView = itemView.contact_photo
        val contactAddress: AppCompatTextView = itemView.contact_address
        val contactTime: AppCompatTextView = itemView.contact_time_from_current_location

        fun bind(contact: Contact, position: Int) {
            contactName.text = contact.fullName
            if (contact.photoUri != null) {
                Glide.with(contactPhoto)
                    .load(contact.photoUri)
                    .apply(RequestOptions.circleCropTransform())
                    .into(contactPhoto)
            } else {

                val generator: ColorGenerator = ColorGenerator.MATERIAL

                val textDrawable = TextDrawable.builder()
                    .buildRound(contact.fullName[0].toString(), generator.getColor(contact.primaryPhoneNumber))

                contactPhoto.setImageDrawable(textDrawable)

            }
            if (contact.address.isNullOrEmpty()) {
//                contactAddress.visibility = View.INVISIBLE
//                contactAddress.currentTextColor
                contactAddress.text = BeepBeepApplication.applicationContext().resources.getString(R.string.must_have_address)
            } else {
//                contactAddress.visibility = View.VISIBLE
                contactAddress.text = contact.address
            }
        }

    }

}