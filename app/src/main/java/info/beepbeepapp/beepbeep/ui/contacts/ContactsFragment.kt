package info.beepbeepapp.beepbeep.ui.contacts

import android.os.Bundle
import android.os.Parcelable
import android.view.*
import android.widget.TextView
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.DaggerFragment
import info.beepbeepapp.beepbeep.R
import info.beepbeepapp.beepbeep.model.Contact
import info.beepbeepapp.beepbeep.ui.MainActivity
import info.beepbeepapp.beepbeep.util.Constant
import kotlinx.android.synthetic.main.contacts_fragment.*
import timber.log.Timber
import javax.inject.Inject

class ContactsFragment : DaggerFragment(), androidx.appcompat.view.ActionMode.Callback, ContactsAdapter.ContactsListListener,
    SearchView.OnQueryTextListener {

    companion object {
        val RECYCLER_VIEW_SAVE_STATE_KEY = "recycler_view_save_state_key"
//        val IS_IN_ACTION_MODE_KEY = "is_in_action_mode_key"
        fun newInstance() = ContactsFragment()
    }

    private lateinit var viewModel: ContactsViewModel
    private var actionMode: ActionMode? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.contacts_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ContactsViewModel::class.java)

        contacts_list.layoutManager = LinearLayoutManager(context)
        savedInstanceState?.let {
            val savedRecyclerState: Parcelable? = savedInstanceState.getParcelable(RECYCLER_VIEW_SAVE_STATE_KEY)
            contacts_list.layoutManager!!.onRestoreInstanceState(savedRecyclerState)
        }
        contacts_list.itemAnimator = DefaultItemAnimator()
        val contactsAdapter = ContactsAdapter(this)
        contacts_list.adapter = contactsAdapter
        floating_action_button.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelableArrayList(Constant.CONTACTS_TO_SUMMARY_FRAGMENT_KEY, viewModel.selectedContactsList.value)
            Navigation.findNavController(activity as MainActivity, R.id.nav_host_fragment).navigate(R.id.action_contactsFragment_to_summaryFragment, bundle)
        }

        viewModel.contactsList.observe(this, Observer {
            contactsAdapter.submitList(it)
        })
        viewModel.selectedContactsList.observe(this, Observer {
            if (it.size > 0) {
                floating_action_button.show()
                (contacts_list.adapter as ContactsAdapter).updateSelectedContacts(it)
                if (actionMode == null) actionMode = (activity as MainActivity).startSupportActionMode(this)!!
                val toolbarTitle = actionMode!!.menu.findItem(R.id.action_search).actionView.findViewById<TextView>(R.id.toolbar_title)
                toolbarTitle.text = "(" + it.size.toString() + "/" + Constant.MAX_NUM_OF_SELECTED_CONTACTS + ")"
            } else {
                floating_action_button.hide()
                actionMode?.finish()
            }
        })
        viewModel.loading.observe(this, Observer {
            if (it) loading_indicator.visibility = View.VISIBLE else loading_indicator.visibility = View.GONE
        })
    }

    override fun contactSelected(selectedContacts: ArrayList<Contact>) {
        viewModel.selectedContactsList.value = selectedContacts
    }

    override fun contactClicked(contact: Contact) {
        val bundle = Bundle()
        bundle.putParcelable(Constant.EDIT_CONTACT_KEY, contact)
        Navigation.findNavController(activity as MainActivity, R.id.nav_host_fragment).navigate(R.id.action_contactsFragment_to_editContactFragment, bundle)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPause() {
        Timber.d("contactsFragment onPause")
        super.onPause()
    }

    override fun onStop() {
        Timber.d("contactsFragment onStop")
        super.onStop()
    }

    override fun onDestroy() {
        Timber.d("contactsFragment onDestroy")
        super.onDestroy()
    }

    override fun onDestroyView() {
        actionMode?.finish()
        Timber.d("contactsFragment onDestroyView")
        super.onDestroyView()
    }

    override fun onDetach() {
        Timber.d("contactsFragment onDetach")
        super.onDetach()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        Timber.d("contactsFragment onSaveInstanceState")
        // Save list position
        outState.putParcelable(RECYCLER_VIEW_SAVE_STATE_KEY, contacts_list?.layoutManager?.onSaveInstanceState())
        super.onSaveInstanceState(outState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.contacts_list_toolbar, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_search) {
            if (actionMode == null) actionMode = (activity as MainActivity).startSupportActionMode(this)!!
//            item.actionView.findViewById<SearchView>(R.id.toolbar_search_view).setIconifiedByDefault(false)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActionItemClicked(mode: androidx.appcompat.view.ActionMode?, item: MenuItem?): Boolean {
        return false
    }

    override fun onCreateActionMode(mode: androidx.appcompat.view.ActionMode?, menu: Menu?): Boolean {
        val inflater = mode!!.menuInflater
        inflater.inflate(R.menu.search_view_toolbar, menu)

        val searchViewLayout = mode.menu.findItem(R.id.action_search).actionView
        val searchView = searchViewLayout.findViewById<SearchView>(R.id.toolbar_search_view)
        searchView.setIconifiedByDefault(false)
        searchView.setOnQueryTextListener(this)

        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        viewModel.queryContacts(newText!!)
        return true
    }

    override fun onPrepareActionMode(mode: androidx.appcompat.view.ActionMode?, menu: Menu?): Boolean {
        return false
    }

    override fun onDestroyActionMode(mode: androidx.appcompat.view.ActionMode?) {
        if (isAdded) {
            (contacts_list.adapter as ContactsAdapter).clearSelectedContacts()
            //TODO Find fix for ActionMode being destroyed on rotation change. When we exit action mode selected contacts are still in the viewmodel, we need to clear them.
//            viewModel.selectedContactsList.value = arrayListOf()
        }
        actionMode = null
    }

}
