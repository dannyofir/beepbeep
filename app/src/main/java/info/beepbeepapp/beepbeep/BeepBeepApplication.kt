package info.beepbeepapp.beepbeep

import android.app.Application
import android.content.Context
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class BeepBeepApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .create(this)
            .build()
        return applicationComponent
    }

    private lateinit var applicationComponent: ApplicationComponent

    init {
        instance = this
    }

    companion object {
        lateinit var instance : BeepBeepApplication
//        var instance : BeepBeepApplication? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}