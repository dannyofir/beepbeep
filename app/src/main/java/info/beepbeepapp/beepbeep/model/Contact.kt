package info.beepbeepapp.beepbeep.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "contacts")
data class Contact(
    @PrimaryKey
    val id: String,
    val fullName: String,
    val phoneNumbers: List<String>,
    val primaryPhoneNumber: String,
    val photoUri: String?) : Parcelable {
    var address: String? = null
    var timeBeforePickupAlert: Int = 3
}