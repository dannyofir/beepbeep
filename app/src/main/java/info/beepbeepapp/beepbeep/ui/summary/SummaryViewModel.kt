package info.beepbeepapp.beepbeep.ui.summary

import androidx.lifecycle.MutableLiveData
import info.beepbeepapp.beepbeep.base.BaseViewModel
import info.beepbeepapp.beepbeep.model.Contact
import javax.inject.Inject

class SummaryViewModel @Inject constructor() : BaseViewModel() {
    // TODO: Implement the ViewModel

    val contacts = MutableLiveData<MutableList<Contact>>()

}
