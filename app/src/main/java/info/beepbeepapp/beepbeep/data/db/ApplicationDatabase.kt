package info.beepbeepapp.beepbeep.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import info.beepbeepapp.beepbeep.model.Contact

@Database(entities = [Contact::class], version = 1)
@TypeConverters(Converters::class)
abstract class ApplicationDatabase : RoomDatabase() {
    abstract fun contactDao(): ContactDao
}