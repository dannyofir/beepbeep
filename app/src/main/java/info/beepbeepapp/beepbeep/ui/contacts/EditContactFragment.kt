package info.beepbeepapp.beepbeep.ui.contacts

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import dagger.android.support.DaggerFragment

import info.beepbeepapp.beepbeep.R
import info.beepbeepapp.beepbeep.model.Contact
import info.beepbeepapp.beepbeep.ui.MainActivity
import info.beepbeepapp.beepbeep.util.Constant
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.edit_contact_fragment.*
import javax.inject.Inject

class EditContactFragment : DaggerFragment() {

    companion object {
        fun newInstance() = EditContactFragment()
    }

    private lateinit var viewModel: EditContactViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.edit_contact_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(EditContactViewModel::class.java)
        // TODO: Use the ViewModel
        viewModel.contact.value = arguments?.getParcelable(Constant.EDIT_CONTACT_KEY)
        viewModel.contact.observe(this, Observer {
            displayPhoto(it)
            contact_name.text = it.fullName
            contact_phone.text = it.primaryPhoneNumber
            contact_address.setText(it.address)
            contact_time_before_pickup_alert_edit_text.setText(it.timeBeforePickupAlert.toString())
        })
        viewModel.saving.observe(this, Observer {
            if (it) {
                loading_indicator.visibility = View.VISIBLE
            } else {
                loading_indicator.visibility = View.GONE
                Toast.makeText(activity, "Contact Edited!", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun displayPhoto(contact: Contact) {
        if (contact.photoUri != null) {
            Glide.with(contact_photo)
                .load(contact.photoUri)
                .apply(RequestOptions.circleCropTransform())
                .into(contact_photo)
        } else {

            val generator: ColorGenerator = ColorGenerator.MATERIAL

            val textDrawable = TextDrawable.builder()
                .buildRound(contact.fullName[0].toString(), generator.getColor(contact.primaryPhoneNumber))

            contact_photo.setImageDrawable(textDrawable)

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.edit_contact_toolbar, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_save) {
            val contact = viewModel.contact.value
            contact?.address = contact_address.text.toString()
            contact?.timeBeforePickupAlert = (contact_time_before_pickup_alert_edit_text.text.toString()).toInt()
            viewModel.editContact()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).bottom_nav.visibility = View.GONE
    }

    override fun onDestroyView() {
        (activity as MainActivity).bottom_nav.visibility = View.VISIBLE
        super.onDestroyView()
    }

}
